class Transformer {
    constructor(name, health = 100) {
        this.name = name;
        this.health = health;
    }
    attack() {
    }
    hit(weapon) {
        this.health -= weapon.damage;
    }
}
class AutoBot extends Transformer {
    constructor(name, weapon) {
        super(name);
        this.weapon = weapon;
    }
    attack() {
        return this.weapon.fight();
    }
}
class Decepticon extends Transformer {
    attack() {
        return {damage: 5, speed: 1000}
    }
}
class Weapon {
    constructor(damage, speed) {
        this.damage = damage;
        this.speed = speed;
    }
    fight() {
        return {damage: this.damage, speed: this.speed}
    }
}
// const transformer1 = new AutoBot('OptimusPrime', new Weapon(100, 1000));
// console.log(transformer1);
// const transformer2 = new Decepticon('Megatron', 10000);
// console.log(transformer2);
class Arena {
    constructor(side1, side2, visualizer) {
        this.side1 = side1;
        this.side2 = side2;
        this.visualizer = visualizer;
    }
    /**
     * Шаг 1 - сторона 1 атакует сторону 2
     * Шаг 2 - проверка условия победы стороны 1
     * Шаг 3 - сторона 2 атакует сторону 1
     * Шаг 4 - проверка условия победы стороны 2
     *
     * для простоты - каждая сторона атакует первого бота другой стороны
     */
    /*start() {
        const timer = setInterval(() => {
            // Шаг 1
            this.side1.forEach(bot => this.findMaxHealthBot(this.side2).hit(bot.attack()));
            // Шаг 2
            this.side2 = this.side2.filter(bot => bot.health > 0);
            if (!this.side2.length) {
                console.log('Сторона 1 победила!');
                clearInterval(timer);
            }
            // Шаг 3
            this.side2.forEach(bot => this.findMaxHealthBot(this.side1).hit(bot.attack()));
            // Шаг 4
            this.side1 = this.side1.filter(bot => bot.health > 0);
            if (!this.side1.length) {
                console.log('Сторона 2 победила!');
                clearInterval(timer);
            }
            this.visualizer.render(this.side1.map(bot => bot.health), this.side2.map(bot => bot.health))
        }, 1000)
    }*/
    /**
     * Каждый бот атакует противника пока: (его здоровье > 0) или  (на стороне противника не осталось ботов)
     */
    startFightWithNewStrategy() {
        // будем отрисовывать ботов с заданным интервалом до тех пор, пока какая-то из сторон не победит
        const renderInterval = setInterval(() => {
            this.visualizer.render(this.side1.map(bot => bot.health), this.side2.map(bot => bot.health));
            if (!this.side1.length || !this.side2.length) {
                console.log(`Таймер рендеринга ${renderInterval} очистился !`)
                clearInterval(renderInterval);
            }
        }, 0);
        // атакуем первой стороной вторую
        this.fight(this.side1, this.side2);
        // атакуем второй стороной первую
        this.fight(this.side2, this.side1);
    }
// метод, в котором s1 - атакующая сторона, s2 - сторона, которую атакуют
    fight(s1, s2) {
        // для каждого бота атакующей стороны прописываем "план атаки" с интервалом = speed:
        // 1. проверить, жив ли бот к моменту наступления атаки
        // 2. проверить, не добил ли его сокомандник сторону противника к моменту наступления атаки
        // 3. атаковать сторону противника - найти самого здорового и ударить
        s1.forEach(bot => {
            const interval = setInterval(() => {
                // если к моменту атаки бота уже убили, очищаем интервал и выходим из функции
                if (!bot || !bot.health || bot.health < 0) {
                    console.log(`Таймер ${interval} очистился ! Бот мертв`);
                    clearInterval(interval);
                    return;
                }
                // проверяем "жива" ли сторона противника, если нет - очищаем интервал, выходим из функции, атаки закончились
                if (!s2.length) {
                    console.log(`Таймер ${interval} очистился ! Победа`)
                    clearInterval(interval);
                    return;
                }
                // если дошли до этой строки - бот жив и победа еще не произошла - атакуем!
                // находим бота с максимальным здоровьем
                const botFromSide2 = this.findMaxHealthBot(s2);
                if (botFromSide2) {
                    botFromSide2.hit(bot.attack());
                    // выводим информацию по атаке
                    console.log(`Атака бота ${bot?.name} - ${bot?.health}, здоровье противника ${botFromSide2.name} - ${botFromSide2.health}`, bot.attack().speed);
                    // "уносим убитых с поля боя"
                    if (s2 === this.side1) {
                        s2 = this.side1 = s2.filter(bot => bot.health > 0);
                    }
                    if (s2 === this.side2) {
                        s2 = this.side2 = s2.filter(bot => bot.health > 0);
                    }
                }
            }, bot.attack().speed) // задаем количество секунд до следующего удара каждого бота
        });
    }
    findMaxHealthBot(array) {
        let botWithMaxHealth = array[0];
        array.forEach(element => {
            if (botWithMaxHealth.health < element.health) {
                botWithMaxHealth = element;
            }
        });
        return botWithMaxHealth;
    }
}
// <div class="bot">
//     <span>100 hp</span>
// </div>
class Visualizer {
    constructor() {
    }
    render(healthSide1, healthSide2) {
        const side1El = document.querySelector('.js-arena-side-1');
        const side2El = document.querySelector('.js-arena-side-2');
        side1El.innerHTML = healthSide1.map(health => `<div class="bot"><span>${health} hp</span></div>`).join('');
        side2El.innerHTML = healthSide2.map(health => `<div class="bot"><span>${health} hp</span></div>`).join('');
    }
}
const area = new Arena(
    [
        new AutoBot('Bot1', new Weapon(100, 1000)),
        new AutoBot('Bot2', new Weapon(100, 2000)),
        new AutoBot('Bot3', new Weapon(100, 3000))
    ],
    [
        new Decepticon('Megatron', 10000)
    ],
    new Visualizer()
);
area.startFightWithNewStrategy();





